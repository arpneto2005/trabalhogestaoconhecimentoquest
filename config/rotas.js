const express = require('express');
const router = express.Router();

const RespostaClass = require('../model/RespostaClass');

router.get('/', function(req, res, next){
    GaleriaModel.getTodos(function(error, retorno){
        let resposta = new RespostaClass();
        if (error){
            resposta.erro = true;
            resposta.msg = 'Ocorreu um erro';
            console.log('Erro', + error);
        }else{
            resposta.dados = retorno;
        }
        res.json(resposta);
    });
});

router.post('/', upload.single('arquivo') , function(req, res, next){
    let resposta = new RespostaClass();

    if(req.file != null){
        GaleriaModel.adicionar(req.body, function(error, retorno){    
            if (error){
                resposta.erro = true;
                resposta.msg = 'Ocorreu um erro';
                console.log('Erro ', + error);
                deletarArquivo(req.body.caminho);
            }else{
                if(retorno.affectedRows > 0){
                    resposta.erro = false;
                    resposta.msg = 'Registro deletado com Sucesso';
                }else{
                    resposta.erro = true;
                    resposta.msg = 'Falha ao deletar';
                    console.log('Erro ', + error);
                    deletarArquivo(req.body.caminho);
                }
            }
            console.log('Resposta: ', resposta);
            res.json(resposta);
        });
    }else{
        resposta.erro = true;
        resposta.msg = 'Vídeo não enviado';
        console.log('Erro', + error);
        res.json(resposta);
    }    
});

module.exports = router;