const app = require('express')()
var bodyParser = require('body-parser');

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.get('/', (req, res)=>{
    res.render('index');
});

app.post('/dados', (req, res)=>{
    let dados = req.body;
    console.log(dados);
    if(dados){
        res.redirect('/produtos', {info: dados});
    }else{
        res.render('/');
    }
});

app.listen(3000, ()=>{
    console.log('Servidor on...');
});